import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EstimatorComponent } from './estimator/estimator.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    // AppComponent,
    EstimatorComponent
  ],
  imports: [
    BrowserModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    NgbModule,
    NgbModalModule
  ],
  providers: [],
  bootstrap: [EstimatorComponent]
})
export class AppModule { }
