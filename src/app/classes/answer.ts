export class Answer {
    title: string;
    value: number;
    goToQuestion: number = null;

    constructor(title: string, value: number, goToQuestion?: number) {
        this.title = title;
        this.value = value;
        if (goToQuestion) this.goToQuestion = goToQuestion;
    }
}