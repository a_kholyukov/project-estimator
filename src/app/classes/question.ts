import { Answer } from "./answer";

export enum OperationType {
    Addition = 1,
    Multiplication
}

export class Question {
    order: number;
    title: string;
    isMultiple: boolean;
    operationType: OperationType;
    variants: Answer[];

    constructor(order: number, title: string, variants: Answer[], multiplicity: boolean = false, operationType: OperationType = OperationType.Addition) {
        this.order = order;
        this.title = title;
        this.variants = variants;
        this.isMultiple = multiplicity;
        this.operationType = operationType;
    }
}

export const QUESTIONS: Question[] = [
    new Question(1, "What registration type do you want?", [new Answer("Via SMS", 8), new Answer("Via E-mail", 8)], true),
    new Question(2, "Do you need social login?", [new Answer("Google", 6), new Answer("Facebook", 6), new Answer("Twitter", 6), new Answer("Instagram", 6)], true),
    new Question(3, "How many fields do you need in profile?", [new Answer("Less than 5", 8), new Answer("From 5 to 10", 12), new Answer("More than 10", 16)]),
    new Question(4, "What search partner logic do you prefer for the App?", [new Answer("Cards swipping", 40), new Answer("Search feed", 60), new Answer("People nearby", 32)]),
    new Question(5, "What monetization type do you need?", [new Answer("In-App purchases", 40), new Answer("Advertisment", 24), new Answer("None", 0)]),
    new Question(6, "Which payment system do you need?", [new Answer("Paypal", 16), new Answer("Strype", 20)]),
    new Question(7, "Do you need localization of user interface?", [new Answer("Yes", 12), new Answer("No", 0)]),
    new Question(8, "Do you need chat in application?", [new Answer("Yes", 50), new Answer("No", 0, 12)]),
    new Question(9, "What types of files will you send in chat?", [new Answer("Text", 20), new Answer("Images", 30), new Answer("Video", 30), new Answer("Audio", 30)], true),
    new Question(10, "Do you need geo-attachments in chat?", [new Answer("Yes", 20), new Answer("No", 0)]),
    new Question(11, "Do you need voice calls in chat?", [new Answer("Yes", 40), new Answer("No", 0)]),
    new Question(12, "Do you need 'Hide profile' feature?", [new Answer("Yes", 16), new Answer("No", 0)]),
    new Question(13, "Do you need admin moderation feature?", [new Answer("Yes", 24), new Answer("No", 0)]),
    new Question(14, "Do you need search/filtering feature?", [new Answer("Yes", 32), new Answer("No", 0)]),
    new Question(15, "Do you need additional third-party integrations?", [new Answer("Instagram", 20), new Answer("Google Maps", 24)], true),
    new Question(16, "Do you need notifications from application?", [new Answer("Push notifications", 20), new Answer("SMS", 16), new Answer("E-mail", 16)], true),
    new Question(17, "Which premium features do you need?", [new Answer("Unlimited likes", 8), new Answer("Top of search cards", 8), new Answer("Anonymous mode", 8), new Answer("Messages to unmatched users", 8)], true),
    new Question(18, "Which mobile platform do you need?", [new Answer("Andorid", 1.2), new Answer("iOS", 1), new Answer("iOS & Android", 1.8)], false, OperationType.Multiplication),
]