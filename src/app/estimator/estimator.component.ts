import { Component, OnInit, ElementRef } from '@angular/core';
import { Question, QUESTIONS, OperationType } from '../classes/question';
import { Answer } from '../classes/answer';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

const dollars = 25;

@Component({
  selector: 'app-estimator',
  templateUrl: './estimator.component.html',
  styleUrls: ['./estimator.component.css']
})
export class EstimatorComponent implements OnInit {

  totalPrice: number;
  currentQuestion: Question;
  questions = QUESTIONS;

  choosenVariantsCount: number;
  previousChoosenValue: number;
  currentAnswer: Answer;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.resetQuestions();
  }

  showModalWindow(content): void {
    this.resetQuestions();
    this.modalService.open(content, { ariaLabelledBy: 'exampleModalCenterTitle', size: 'lg', centered: true, windowClass: 'estimator-fade' });
  }

  goToNextQuestion() {
    var nextQuestion;
    if (this.currentAnswer == null || this.currentAnswer.goToQuestion == null) {
      nextQuestion = this.questions.find(el => el.order === this.currentQuestion.order + 1);
    }
    else {
      nextQuestion = this.questions.find(el => el.order === this.currentAnswer.goToQuestion);
    }
    this.currentQuestion = nextQuestion == null ? null : nextQuestion;
    this.previousChoosenValue = 0;
    this.choosenVariantsCount = 0;
    this.currentAnswer = null;
  }

  resetQuestions() {
    this.currentQuestion = this.questions.find(el => el.order === 1);
    this.totalPrice = 0;
    this.choosenVariantsCount = 0;
    this.previousChoosenValue = 0;
    this.currentAnswer = null;
  }

  onClickRadio(answer: Answer) {
    if (this.choosenVariantsCount === 0) this.choosenVariantsCount++;
    this.currentAnswer = answer;
    switch (this.currentQuestion.operationType) {
      case OperationType.Addition: {
        this.totalPrice -= this.previousChoosenValue * dollars;
        this.previousChoosenValue = answer.value;
        this.totalPrice += answer.value * dollars;
      }
      case OperationType.Multiplication: {
        this.totalPrice /= this.previousChoosenValue == 0 ? 1 : this.previousChoosenValue;
        this.previousChoosenValue = answer.value;
        this.totalPrice *= answer.value == 0 ? 1 : answer.value;
      }
    }
  }

  onClickCheckbox(event: any, value: number) {
    if (event.target.checked) {
      this.choosenVariantsCount++;
      this.totalPrice += value * dollars;
    }
    else {
      this.choosenVariantsCount--;
      this.totalPrice -= value * dollars;
    }
  }
}
